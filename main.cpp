#include<bits/stdc++.h>

using namespace std;

 typedef long long ll;

#include<bits/stdc++.h>

using namespace std;

 typedef long long ll;

struct task
{
    int id; //id processus
    int Ta; // temps d�arriv�e
    int duree; //dur�e du cycle
    int priority;//priorité

    };


/*
    * temps d'attent et rotation moyen
*/

 void tempsRotationAndAttente(vector<task> P){
        float trm = 0.0, tam = 0.0, tfTotal = 0.0;
        ll taTotal = 0;
        for(int i =0; i<P.size(); i++){
            tfTotal+= P[i].duree;
            taTotal +=P[i].Ta;
            trm+= (tfTotal - P[i].Ta);
        }
        tam = (trm - tfTotal)/P.size();
        trm /= P.size();
        printf("|ID | temp d'arrivé | durée|\n");
        printf("|___|_______________|______|\n");
        for(int i =0; i<P.size(); i++){
                      printf("|P%d:|\t%d    \t|    %d\n",P[i].id, P[i].Ta, P[i].duree);

        }
        printf("temps de rotation moyen : %f\n", trm);
        printf("temps d'attente moyen : %f\n", tam);
 }



    /*
        * SJF
    */


int shortJob(vector<task> P, int duree){
   int pos = 0, i=0;
   while(P[i].Ta<=duree && i<P.size()){
        if(P[i].duree < P[pos].duree){
            pos = i;
        }
        i++;
   }
    return pos;
}



 void sjf(vector<task> tab){
     vector<task> P;
     ll extime = 0;
     int pos;
     float trm = 0.0, tam = 0.0, tfTotal = 0.0;
     ll taTotal = 0;
     P.insert(P.begin(), tab.begin(), tab.end());

        sort(P.begin(), P.end(), [](task P1, task P2){
            return P1.Ta < P2.Ta || (P1.Ta == P2.Ta && P1.duree <= P2.duree);
        });
        /*
        for(int i =0; i<n; i++){
            printf("%d\t%d\n", P[i].start, P[i].duree);
            }
        cout<<"**************"<<endl;*/
        printf("|ID | temp d'arrivé | durée|\n");
        printf("|___|_______________|______|\n");
     while (P.size()){
          printf("|P%d:|\t%d    \t|    %d\n",P[0].id, P[0].Ta, P[0].duree);
            extime+=P[0].duree;
            tfTotal+=P[0].duree;
            trm+=(tfTotal - P[0].Ta);
            P.erase(P.begin());
            if (extime>= P[P.size()-1].Ta){
                    sort(P.begin(), P.end(), [](task P1, task P2){return P1.duree<P2.duree;});

                for(int i= 0; i< P.size(); i++){
                    tfTotal+=P[i].duree;
                    trm+=(tfTotal - P[i].Ta);
                    printf("|P%d:|\t%d    \t|    %d\n",P[i].id, P[i].Ta, P[i].duree);
                }
                break;
            }
            pos = shortJob(P, extime);
            swap(P[0], P[pos]);
     }
    printf("temps de rotation moyen: %f\n",trm/tab.size());
    printf("temps d'attente moyen: %f\n",(trm - tfTotal)/tab.size());
 }

 /*
    * FIFO
 */

 void fifo(vector<task> P){
        sort(P.begin(), P.end(), [](task P1, task P2){
            return P1.Ta < P2.Ta;
        });
        tempsRotationAndAttente(P);
 }



   /////////////Round Robin///////////////////////
    void RoundRobin(vector<task> tasks,int q)
    {
        cout<<"ID"<<"      |"<< " temps systeme "<<"| temp d'arrivé | durée|"<<endl;
                cout<<"________|_______________|_______________|______|"<<endl;
        int cycle=0;
        deque<task> current;
        int k=0;
         unsigned int i=0;
        while (k<tasks.size() || current.size()>0){
            int j=q;

      //un nouveau processus est ajouté en fin de liste
            while (tasks[k].Ta<=cycle&& k<tasks.size()){

                if(current.empty()|| cycle==tasks[k].Ta){
                    current.push_back(tasks[k]);
                    k++;
               }else{
                    task T =current.back();
                    current.pop_back();
                    current.push_back(tasks[k]);
                    current.push_back(T);
                    k++;
                }

            }



           //execution de processus


              if (!current.empty()){
                if (current.front().duree<=q){
                    cout<<"p"<< current.front().id<<":     |      "<< cycle<<"\t|    "<<current.front().Ta<<"\t\t|"<<current.front().duree<<endl;
                    cycle+=current.front().duree;
                    //un processus qui a terminé son travail est sorti de la liste
                    current.pop_front();
                    i--;
                    j-=q;
                }else{
                    cout<<"p"<< current.front().id<<":     |      "<< cycle<<"\t|    "<<current.front().Ta<<"\t\t|"<<current.front().duree<<endl;
                    cycle+=q;
                    current.front().duree-=q;
                    //un processus qui vient de finir d'utiliser le processeur (quantum écoulé) est placé en fin de liste
                    current.push_back(current.front());
                    current.pop_front();
                    j-=q;
                }}








            cycle+=j;


        }

    }



    void saisie(vector<task> &tasks){
        task proc;
        char c='o';
        int i=0;
        while (toupper(c)=='O'){
            cout<<"id= "<<tasks.size() <<endl;
            proc.id=tasks.size();
            cout<<"Ta= ";
            cin>> proc.Ta;
            cout<<"duree= ";
            cin>> proc.duree;
          
            tasks.push_back(proc);

            do {
            cout<<"Ajouter un autre processus ? (O/N)";
                cin >>c;
            }while (toupper(c)!='O' && toupper(c)!='N' );
            i++;
        }

        sort(tasks.begin(), tasks.end(), [](task P1, task P2){
            return P1.Ta < P2.Ta;
        });
        cout<<"\033[2J\033[1;1H";


    }
        void readfile (vector<task> &tasks){
                                int n;
                                task proc;
                                string s;
                   // cout <<"donner le chemin du fichier $ ";
                    //cin >> s;
                    ifstream lire ("text100.txt");

                        lire >>n;
                        cout <<n<<endl;

                        for (int i=0;i<n;i++){
                          //  cin>> proc.id;
                            proc.id=tasks.size();
                            lire>> proc.Ta;
                            lire>> proc.duree;
                            tasks.push_back(proc);

                        }
        }

    void Loading (vector<task> &tasks){
       int i;
        cout <<"------------------- Ajout des processus ------------------"<<endl;
          cout <<"1)      Charger un Fichier"<<endl;
          cout<<endl;
          cout <<"2)      Processus Aléatoire"<<endl;
          cout <<endl;
          cout <<"3)      Ajout manuel"<<endl;
          cout <<endl;
          cout <<"4)      Supprimer tous les processus"<<endl;
          cout <<endl;
          cout << "charger la: ";
          cin  >>i;
          cout<<"\033[2J\033[1;1H";
            task proc;

          switch(i){
            case 1: cout << "--------------------- Charger un Fichier ------------------"<<endl;
                       readfile(tasks);
                    break;
            case 2: cout << "--------------------- Processus Aléatoire ------------------"<<endl;
                    int k;
                    cout<<"Veuillez donner le nombre de processus à génerer= ";
                    cin >>k;
                    for (int f=0;f<k;f++){
                    proc.id=tasks.size();
                    proc.duree=rand()%(k/2)+1;
                    proc.Ta=rand()%(k/2);
                    tasks.push_back(proc);
                    }

                    break;
            case 3: cout << "--------------------- Ajout manuel ------------------"<<endl;
                        saisie(tasks);
                    break;
            case 4: tasks.clear();
                    break;
            default: Loading(tasks);
                    break;
          }
          cout<<"\033[2J\033[1;1H";
        sort(tasks.begin(), tasks.end(), [](task P1, task P2){
            return P1.Ta < P2.Ta;
        });

    }

    void affiche(vector<task> tasks){
    for (unsigned int i=0; i< tasks.size();i++){
        cout<<tasks[i].id<<endl;
    }}

main ( int argc, char **argv ) {
   vector<task> tasks;

 char c='o';
        int i=0;
        while (toupper(c)=='O'){
                cout <<"---------------------------Menu Principale-------------------------"<<endl;
cout<<endl;
        cout <<"1)      non préemptif méthode 1  (Shortest Job First)"<<endl;
          cout<<endl;
          cout <<"2)      non préemptif  méthode2 (First In First Out)"<<endl;
          cout <<endl;
          cout <<"3)      préemptif (Round Robin)"<<endl;
          cout <<endl;
          cout <<"4)      Ajouter/supprimer des processus "<<endl;
          cout <<endl;
          cout << "Veuillez Choisir la politique à executer: ";
          cin  >>i;
          cout<<"\033[2J\033[1;1H";


           switch(i)    {
            case 1: cout << "---------- non préemptif méthode 1 (Shortest Job First)--------"<<endl;
                    if (tasks.empty()){Loading(tasks);}
                    sjf(tasks);
                    break;
            case 2: cout << "--------------- non préemptif 2 (First In First Out) ----------"<<endl;
                    if (tasks.empty()){Loading(tasks);}
                    fifo(tasks);
                    break;
            case 3: cout << "-------------------- préemptif (Round Robin)-------------------"<<endl;
                    if (tasks.empty()){Loading(tasks);}
                    int k;
                    cout<<"choisir le quantum du temps= ";
                    cin >>k;

                    RoundRobin(tasks,k);
                    break;
            case 4: Loading(tasks);
           }


         do {
            cout<<"Continuer (O/N)? ";
                cin >>c;
                if (toupper(c)=='N' ){return 0;}
            }while (toupper(c)!='O' && toupper(c)!='N' );
             cout<<"\033[2J\033[1;1H";
        }


    return 0;
}
